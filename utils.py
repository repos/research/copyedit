import requests
import re
import json
import nltk 
from bs4 import BeautifulSoup # necessary for parsing HTML

def map_lang_lt(lang):
    '''
    Map wikipedia-project's language to the language used by LanguageTool.
    For example, simple (from simplewiki) gets mapped to en.
    '''
    dict_map_lang = {"simple":"en"}
    # if there is no entry in the dictionary, we return the input lang
    lang_lt = dict_map_lang.get(lang,lang)
    return lang_lt

def map_lang_enchant(lang):
    '''
    Map wikipedia-project's language to the language used by Enchant.
    For example, simple (from simplewiki) gets mapped to en_US.
    In enchant, one can get the available languages via enchant.list_languages()
    '''
    dict_map_lang = {
        "bn": "bn_BD",
        "cs": "cs_CZ",
        "de": "de_DE",
        "en": "en_US",
        "gl": "gl_ES",
        "it": "it_IT",
        "pt": "pt_BR",
        "ru": "ru_RU",
        "simple": "en_US",

    }
    # if there is no entry in the dictionary, we return the input lang
    lang_enchant = dict_map_lang.get(lang,lang)
    return lang_enchant

def map_match2dict(match):
    dict_match = {
        "ruleId":match.ruleId,
        "message":match.message,
        "context":match.context,
        "matchedText":match.matchedText,
        "replacements":match.replacements,
        "offsetInContext":match.offsetInContext,
        "offset":match.offset,
        "errorLength":match.errorLength,
        "category":match.category,
        "ruleIssueType":match.ruleIssueType,
        "sentence":match.sentence,

    }
    return dict_match

def map_match2dict_enchant(match, suggest = True):
    suggestions = []
    if suggest==True:
        try:
            suggestions = match.suggest()
        except:
            pass
    dict_match = {
        "word":match.word,
        "wordpos":match.wordpos,
        "suggestions":suggestions,
    }
    return dict_match

def errant_eval2dict(result_str):
    """
    return a dictionary with key-value pairs of the evaluation metrics from the string-result
    
    """
    labels = result_str.split("\n")[2].split("\t")
    vals = [int(h) if i<3 else float(h) for i,h in enumerate(result_str.split("\n")[3].split("\t")) ]
    result_dict = dict(zip(labels,vals))
    return result_dict

def languagetool_match2edits(matches):
    """
    get all matches from languagetool and return list of edits:
    - characteroffset-start
    - characteroffset-end
    - replacement (if no, we add NONE)
    """
    edits = []
    for match in matches:
        i1 = match.offset
        i2 = i1+match.errorLength
        edit = "NONE"
        try:
            edit = match.replacements[0]
        except:
            pass
        edits += [[i1,i2,edit]]
    return edits

def enchant_match2edits(matches):
    """
    get all matches from languagetool and return list of edits:
    - characteroffset-start
    - characteroffset-end
    - replacement (if no, we add NONE)
    """
    edits = []
    for match in matches:
        i1 = match.wordpos
        i2 = i1+len(match.word)
        edit = "NONE"
        try:
            edit = match.suggest()[0]
        except:
            pass
        edits += [[i1,i2,edit]]
    return edits

def get_article_html(page_title, lang):
    """
    Get the HTML-version of an article of a Wikipedia.
    """
    headers = {"User-Agent": "MGerlach_(WMF): research-copyediting"}
    api_url = "https://%s.wikipedia.org/w/api.php"%lang
    params = {
        "action": "parse",
        "page": page_title,
        "format": "json",
    }
    article_html = ""
    try:
        response = requests.get(api_url, headers=headers, params=params)
        req = response.json()
        article_html = req["parse"]["text"]["*"]
    except:
        pass
    return article_html

def html2sentences(article_html):
    '''
    Convert html to a list of sentences.
    '''
    soup = BeautifulSoup(article_html, 'html.parser')
    list_p = soup.find_all('p')
    list_sentences = []
    for p in list_p:
        # the html-dump always adds an id which we remove since it is not needed
        try:
            p.attrs.pop("id")
        except KeyError:
            pass
        ## of the piece has an additional attribute it is likely not from the body of the text but translcuded.
        if len(p.attrs) > 0:
            continue

        p_text = ""

        list_pc = []
        i1,i2=-1,0 # index markers for the text elements
        ## iterating through the individual elements of a piece
        for pc in p:
            i1=i2
            ## if it is pure text, the children does not contain the attrbute attrs
            pc_text = pc.text.replace("\n"," ")#.replace("  "," ")
            # replace multiple whitespaces by single to avoid whitespace error
            _RE_COMBINE_WHITESPACE = re.compile(r"\s+")
            pc_text = _RE_COMBINE_WHITESPACE.sub(" ", pc_text)

            if pc_text=="":
                continue

            # get the attributes of the text piece
            pc_attr = None
            pc_name = None
            
            # get the remaining attributes
            try:
                # remove the id attribute
                if "id" in pc.attrs:
                    pc.attrs.pop("id")
                pc_attr = pc.attrs
                pc_name = pc.name
            except AttributeError:
                pass

            # ignore references (and some templates)
            if pc_name == "sup":
                continue
            # style annotations
            if pc_name == "style":
                continue
#                 if "reference" in pc_attr.get("class",[]):            
            i2 = i1+len(pc_text)
            p_text += pc_text
            dict_pc = {"text":pc_text, "attr":pc_attr, "name":pc_name, "i_start":i1, "i_end":i2}
            list_pc += [dict_pc]

        if len(list_pc)>0 and len(p_text)>100:
            list_sentences+=[{"text": p_text, "text_pc": list_pc}]
    return list_sentences

def tokenize_sentence(text):
    '''
    returns individual tokens from a sentence.
    '''
    # full stop symbol in bengali
    text = text.replace("।", ".\n")
    for line in text.split("\n"):
        if len(line)>=1:
            yield line
        # for sent in nltk.sent_tokenize(line):
        #     ## if sentences are very short, they are often just artifacts.
        #     if len(sent)>=10:
        #         yield sent