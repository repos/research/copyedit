import sys,os
import json
import utils
import subprocess
from enchant.checker import SpellChecker

# lang_enchant = "en_US"
lang_enchant = "en_GB"

list_dataset = ["A.train","B.train","C.train"]
# list_dataset = ["A.dev","B.dev","C.dev"]
n_max = -1

for dataset in list_dataset:
    FILENAME_read = "./data/wi+locness/json/%s.json"%dataset
    FILENAME_ref = "./output/wi_%s_ref"%dataset
    FILENAME_hyp = "./output/wi_%s_hyp_enchant_%s"%(dataset,lang_enchant)
    FILENAME_result = "output/enchant_eval-errant_wi-%s_%s_result.json"%(dataset,lang_enchant)
    print("Using the following dataset from W&I: ", dataset)



    print("Generating the reference dataset.")
    # simply write data to separate file
    n_processed = 0
    with open(FILENAME_ref+".json","w") as fout:
        with open(FILENAME_read) as fin:
            for line in fin:
                x_json = json.loads(line)
                text = x_json['text']
                # only texts that are at most 5000; otherwise there is an error when parsing LanguageTool
                # only removes a handful of texts in B/C
                if len(text)<=5000:
                    n_processed += 1
                    fout.write(line)
                if n_processed==n_max:
                    break
    print("Dataset has %s items."%n_processed)

    print("Generating the annotations from LanguageTool.")
    # write a json file with the edits from LanguageTool
#     tool = language_tool_python.LanguageTool(lang_lt, remote_server='https://copyedits.wmcloud.org')  # use a remote server

    with open(FILENAME_hyp+".json","w") as fout:
        with open(FILENAME_ref+".json") as fin:
            for line in fin:
                x_json = json.loads(line)
                text = x_json['text']
                matches = SpellChecker(lang_enchant,text)
                edits = utils.enchant_match2edits(matches)
                json_out = {"text":text, "edits":[[0,edits]]}
                fout.write(json.dumps(json_out)+"\n")

    # generating m2-files
    print("Generating the m2-files.")
    try:
        x = subprocess.run(["python", "json_to_m2.py", FILENAME_ref+".json", "-out", FILENAME_ref+".m2","-gold"], capture_output=True)
        x = subprocess.run(["python", "json_to_m2.py", FILENAME_hyp+".json", "-out", FILENAME_hyp+".m2","-gold"], capture_output=True)
    except:
        pass

    # # Evaluation
    print("Evaluating the edits.")
    result_correction = ""
    result_detection = ""
    try:
        x = subprocess.run(["errant_compare","-hyp",FILENAME_hyp+".m2","-ref",FILENAME_ref+".m2"], capture_output=True)
        result_correction = utils.errant_eval2dict(x.stdout.decode("utf-8"))

        x = subprocess.run(["errant_compare","-hyp",FILENAME_hyp+".m2","-ref",FILENAME_ref+".m2","-ds"], capture_output=True)
        result_detection = utils.errant_eval2dict(x.stdout.decode("utf-8"))
    except:
        pass


    print("Sanity check for the m2-files.")
    n_sents_ref = 0
    with open(FILENAME_ref+".m2") as fin:
        for line in fin:
            if line[0]=="S":
                text = line[1:].strip()
                n_sents_ref += 1
    print("reference-data: %s sentences"%n_sents_ref)

    n_sents_hyp = 0
    with open(FILENAME_hyp+".m2") as fin:
        for line in fin:
            if line[0]=="S":
                text = line[1:].strip()
                n_sents_hyp += 1
    print("hypothesis-data: %s sentences"%n_sents_hyp)

    
    with open(FILENAME_result,"w") as fout:
        dict_out = {
            "n_items":n_max,
            "n_sents_hyp":n_sents_hyp,
            "n_sents_ref":n_sents_ref,
            "eval_correction":result_correction,
            "eval_detection":result_detection
        }
        fout.write(json.dumps(dict_out))

    print("Error correction", result_correction)
    print("Error detection", result_detection)